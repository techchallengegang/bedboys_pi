// NodeJS SPI Dump for MCP3008 - Created by Mikael Levén
// Original https://gist.github.com/mikaelleven/c3db08ae7837eb3c8698
const admin = require("firebase-admin");
const serviceAccount = require("../config/tc-bedboys-firebase-adminsdk-6e6l3-40b75bb349.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://tc-bedboys.firebaseio.com",
   
});
const db =admin.database();
var rpio = require('rpio');
var gpio = require("gpio");

rpio.spiBegin();
//rpio.spiChipSelect(0);                  /* Use CE0 (slave 0) */
//rpio.spiSetCSPolarity(0, rpio.LOW);    /* Commonly chip enable (CE) pins are active low, and this is the default. */
//rpio.spiSetClockDivider(256);           /* MCP3008 max is ~1MHz, 256 == 0.98MHz */
//rpio.spiSetDataMode(0);

process.stdout.write('\x1b[36m');
for (var channelHeader = 0; channelHeader <= 7; channelHeader++) {
    process.stdout.write('ch' + channelHeader.toString() + (channelHeader == 7 ? '\x1b[0m\n' : '\t'));
}
let compareValue = 99999

setInterval(function() {
    var txBuffer = new Buffer([0x01, (8 + 0 << 4), 0x01]);
    var rxBuffer = new Buffer(txBuffer.byteLength);
    var inBed = undefined;
    rpio.spiTransfer(txBuffer, rxBuffer, txBuffer.length); // Send TX buffer and recieve RX buffer
    var junk = rxBuffer[0],
    MSB = rxBuffer[1],
    LSB = rxBuffer[2];
    let newValue = ((MSB & 3) << 8) + LSB;
    console.log("new value: " + newValue + " | compare value: " + compareValue)
    if (compareValue === 99999) {
        compareValue = newValue
    }
    if (compareValue !== undefined) {
        if (Math.abs(compareValue - newValue) > 300) {
            //console.log("Hey hier hat sich was geändert")
        }
        compareValue = newValue
    
        if (newValue <600){
            inBed = false
            const state = db.ref("sleep/"); 
            var updateinBed = state
            updateinBed.update({"inBed":false})
        } else{
            inBed = true 
            console.log("Ein Faulpelz liegt im Bett ")
        const state = db.ref("sleep/"); 
        var updateinBed = state
        updateinBed.update({"inBed":true})
        }
        


    }
 
    // for (var channel = 0; channel <= 7; channel++) {
    //     // Prepare TX buffer [trigger byte = 0x01] [channel 0 = 0x80 (128)] [placeholder = 0x01]
    //     var txBuffer = new Buffer([0x01, (8 + channel << 4), 0x01]);
    //     var rxBuffer = new Buffer(txBuffer.byteLength);

    //     rpio.spiTransfer(txBuffer, rxBuffer, txBuffer.length); // Send TX buffer and recieve RX buffer

    //     // Extract value from output buffer. Ignore first byte.
    //     var junk = rxBuffer[0],
    //         MSB = rxBuffer[1],
    //         LSB = rxBuffer[2];

    //     // Ignore first six bits of MSB, bit shift MSB 8 positions and
    //     // finally combine LSB and MSB to get a full 10 bit value
    //     var value = ((MSB & 3) << 8) + LSB;
    //     if (value)

    //     process.stdout.write(value.toString() + (channel == 7 ? '\n' : '\t'));
    //};
}, 1000);



process.on('SIGTERM', function () {

    process.exit(0);
});

process.on('SIGINT', function () {
    process.exit(0);
});

process.on('exit', function () {
    console.log('\nShutting down, performing GPIO cleanup');
    rpio.spiEnd();
    process.exit(0);
});
