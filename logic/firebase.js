var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
            try {
                step(generator.next(value));
            }
            catch (e) {
                reject(e);
            }
        }
        function rejected(value) {
            try {
                step(generator["throw"](value));
            }
            catch (e) {
                reject(e);
            }
        }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const admin = require("firebase-admin");
const serviceAccount = require("../config/tc-bedboys-firebase-adminsdk-6e6l3-40b75bb349.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://tc-bedboys.firebaseio.com",
});
var gpio = require("gpio");
// Calling export with a pin number will export that header and return a gpio header instance
var gpioUp = gpio.export(27, {
    direction: gpio.DIRECTION.OUT,
    interval: 200,
    ready: function () { },
});
var gpioDown = gpio.export(17, {
    direction: gpio.DIRECTION.OUT,
    interval: 200,
    ready: function () { },
});
var gpioOk = gpio.export(26, {
    direction: gpio.DIRECTION.OUT,
    interval: 200,
    ready: function () { },
});
var gpioPower = gpio.export(22, {
    direction: gpio.DIRECTION.OUT,
    interval: 200,
    ready: function () { console.log("this is power"); },
});
function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
const db = admin.database();
const remote = db.ref("remote/");
let up = undefined;
let down = true;
let power = undefined;
let ok = undefined;
let speed = undefined;
let amplitude = undefined;
let duration = undefined;
remote.on("value", function (snapshot) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log("####### #### Started ##### # # # ## # #");
        const val = snapshot.val();
        if (up === undefined) {
            up = val.up;
            down = val.down;
            power = val.power;
            ok = val.ok;
        }
        if (val.up !== up) {
            up = val.up;
            console.log("up");
            gpioUp.set();
            gpioUp.set(0);
            gpioUp.set();
        }
        if (val.down !== down) {
            down = val.down;
            console.log("down");
            gpioDown.set();
            gpioDown.set(0);
            gpioDown.set();
        }
        if (val.ok !== ok) {
            ok = val.ok;
            console.log("ok");
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
        }
        if (val.power !== power) {
            power = val.power;
            console.log("power1");
            gpioPower.set();
            gpioPower.set(0);
            gpioPower.set();
            yield timeout(500);
            gpioPower.set();
            gpioPower.set(0);
            gpioPower.set();
        }
    });
});
//var istSpeed = speed;
let istSpeed = undefined;
//var istAmplitude = 1;
let istAmplitude = undefined;
var istDuration = 1;
//let istDuration = undefined;
const state = db.ref("state/");
state.on("value", function (snapshot) {
    return __awaiter(this, void 0, void 0, function* () {
        console.log("####### #### Started ##### # # # ## # #");
        const val = snapshot.val();
        if (speed === undefined) {
            speed = val.speed;
            //amplitude = val.amplitude;
            amplitude = val.amplitude;
            //duration = val.duration;
            duration = val.duration;
        }
        if (val.speed !== istSpeed) {
            //console.log("1. Delta Speed"+ new Date());
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            //console.warn("2. OK Before" + new Date())
            yield timeout(500);
            // console.warn("3. OK Pressed" + new Date())
            var speedDifferenz = val.speed - istSpeed;
            //  console.log("4. Speeddif: " + speedDifferenz +new Date());
            istSpeed = val.speed;
            if (speedDifferenz > 0) {
                yield timeout(500);
                for (var i = 0; i < speedDifferenz; i++) {
                    //  console.log("up"+i);
                    //console.log("speed ist" + speed);
                    // console.log("5. istSpeed ist " + istSpeed + new Date() );
                    gpioUp.set();
                    gpioUp.set(0);
                    gpioUp.set();
                    yield timeout(500);
                    gpioUp.set();
                    gpioUp.set(0);
                    gpioUp.set();
                    yield timeout(500);
                    //setTimeout(() => { console.log("up_done!"); }, 2000);
                }
            }
            if (speedDifferenz < 0) {
                for (var i = 0; i < Math.abs(speedDifferenz); i++) {
                    console.log("down" + i);
                    gpioDown.set();
                    gpioDown.set(0);
                    gpioDown.set();
                    yield timeout(500);
                    gpioDown.set();
                    gpioDown.set(0);
                    gpioDown.set();
                    yield timeout(500);
                    // setTimeout(() => { console.log("down_done!"); }, 2000);
                }
            }
        }
        if (val.amplitude !== istAmplitude) {
            //amplitude = val.amplitude;
            // console.log("6. Delta Amplitude"+ new Date());
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            // await timeout(500)
            //console.warn("7. 3x OK" + new Date())
            var amplitudeDifferenz = val.amplitude - istAmplitude;
            // console.log("####. Ampdif: " + amplitudeDifferenz +new Date());
            istAmplitude = val.amplitude;
            if (amplitudeDifferenz > 0) {
                yield timeout(500);
                for (var i = 0; i < amplitudeDifferenz; i++) {
                    console.log("#up" + i);
                    //console.log("speed ist" + speed);
                    console.log("5. istAmplitude ist " + istAmplitude + new Date());
                    gpioUp.set();
                    gpioUp.set(0);
                    gpioUp.set();
                    yield timeout(500);
                    gpioUp.set();
                    gpioUp.set(0);
                    gpioUp.set();
                    yield timeout(500);
                    //setTimeout(() => { console.log("up_done!"); }, 2000);
                }
            }
            if (amplitudeDifferenz < 0) {
                for (var i = 0; i < Math.abs(amplitudeDifferenz); i++) {
                    console.log("##down" + i);
                    gpioDown.set();
                    gpioDown.set(0);
                    gpioDown.set();
                    yield timeout(500);
                    gpioDown.set();
                    gpioDown.set(0);
                    gpioDown.set();
                    yield timeout(500);
                    // setTimeout(() => { console.log("down_done!"); }, 2000);
                }
            }
        }
        if (val.duration !== duration) {
            duration = val.duration;
            console.log("duration Startet Istduration" + duration);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            yield timeout(500);
            gpioOk.set();
            gpioOk.set(0);
            gpioOk.set();
            console.log("durcschalten");
            var durationDifferenz = val.duration - istDuration;
            console.log("###. Durationdiff: " + durationDifferenz + new Date());
            console.log("istDuration" + istDuration);
            istDuration = val.duration;
            if (durationDifferenz > 0) {
                yield timeout(500);
                for (var i = 0; i < durationDifferenz; i++) {
                    console.log("up" + i);
                    //console.log("speed ist" + speed);
                    console.log("5. istduration ist " + durationDifferenz + new Date());
                    gpioUp.set();
                    gpioUp.set(0);
                    gpioUp.set();
                    yield timeout(500);
                    gpioUp.set();
                    gpioUp.set(0);
                    gpioUp.set();
                    yield timeout(500);
                    //setTimeout(() => { console.log("up_done!"); }, 2000);
                }
            }
            if (durationDifferenz < 0) {
                for (var i = 0; i < Math.abs(durationDifferenz); i++) {
                    console.log("down" + i);
                    gpioDown.set();
                    gpioDown.set(0);
                    gpioDown.set();
                    yield timeout(500);
                    gpioDown.set();
                    gpioDown.set(0);
                    gpioDown.set();
                    yield timeout(500);
                    // setTimeout(() => { console.log("down_done!"); }, 2000);
                }
            }
        }
    });
});
// startBed 
const alarm = db.ref("alarm/");
var alarm1 = require('alarm');
alarm1.recurring(60000, function () {
    console.log("Here we are again");
    alarm.on("value", function (snapshot) {
        const val = snapshot.val();
        console.log("####### #### Started ##### # # # ## # #");
        var weckzeit = val.time.toString();
        console.log(weckzeit.substring(0, 16));
        console.log("trenner");
        console.log(Date());
        var stunde;
        // time Zone issues, as normal time -2 and start bed -1 hour from current time = -3
        stunde = parseInt(weckzeit.substring(11, 13)) - 2;
        //compare first 16 signs of string last four are hour:minute, if equal start Bed
        var minute;
        console.log(stunde);
        if (weckzeit.substring(14, 16) >= 30) {
            minute = parseInt(weckzeit.substring(14, 16)) - 30;
            if (minute < 10) {
                minute = "0" + minute;
                console.log("minute 0+ " + minute);
            }
        }
        else {
            stunde = stunde - 1;
            minute = parseInt(weckzeit.substring(14, 16));
            minute = minute + 30;
        }
        if (stunde < 10) {
            stunde = "0" + stunde;
            console.log("stunde 0+ " + stunde);
        }
        state.on("value", function (snapshot) {
            const powerVal = snapshot.val().power;

            return __awaiter(this, void 0, void 0, function* () {
                var startBett = new Date().toISOString().substring(0, 11) + stunde + ":" + minute;
                //console.log("Uhrzeit Datenbank" + val.time)
                console.log("berechnet" + startBett);
                //console.log("aktuelle Zeit: " + new Date().toISOString().substring(0, 16));
                //if (true) {
                var bedStarted = false;
                console.log(new Date().toISOString().substring(0, 16));
                if (startBett === new Date().toISOString().substring(0, 16)) {
                    // alles auf 1
                    var updateValues = state;
                    updateValues.update({
                        "speed": 1
                    });
                    yield timeout(15000);
                    var updateValues = state;
                    updateValues.update({
                        "amplitude": 1
                    });
                    yield timeout(15000);
                    var updateValues_Remote = remote;
                    updateValues_Remote.update({
                        "power": !powerVal
                        
                    });
                    console.log("power" + powerVal)
                    yield timeout(15000);
                    yield timeout(8000);
                    bedStarted = true;
                }
                if (bedStarted === true) {
                    for (let index = 2; index < 9; index++) {
                        yield timeout(7000);
                        updateValues.update({
                            "speed": index
                        });
                        yield timeout(7000);
                        updateValues.update({
                            "amplitude": index
                        });
                        yield timeout(10000)
                        bedStarted = false;
                    }
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "speed": 2
                    // });
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "amplitude": 2
                    // });
                    // yield timeout(7000);
                    // yield timeout(8000);
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "speed": 3
                    // });
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "amplitude": 3
                    // });
                    // yield timeout(8000);
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "speed": 4
                    // });
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "amplitude": 4
                    // });
                    // yield timeout(8000);
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "speed": 5
                    // });
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "amplitude": 5
                    // });
                    // yield timeout(8000);
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "speed": 6
                    // });
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "amplitude": 6
                    // });
                    // yield timeout(8000);
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "speed": 7
                    // });
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "amplitude": 7
                    // });
                    // yield timeout(8000);
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "speed": 8
                    // });
                    // yield timeout(7000);
                    // updateValues.update({
                    //     "amplitude": 8
                    // });
                    // bedStarted = false;
                }
                //const val = snapshot.val();
            });
        });
    });
});
